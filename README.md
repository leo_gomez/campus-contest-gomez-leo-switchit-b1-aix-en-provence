# Campus Contest - GOMEZ Léo SwitchIT B1 -  Aix-en-Provence

Ceci est mon dépôt Git pour le Campus Contest du 20 au 21 Mai 2021

## Idée de conception initiale :

Avant de commancer à coder mon site, j'ai commencé par chercher différentes idées, finalement l'idée qui m'est restée a été celle que je vous présente ici:
<br>

![Maquette Portfolio](mqtteindexhtml.png)

<br>

Je voulais d'abord quand on entre sur le site faire apparaitre mon "titre" (*Etudiant en Switch IT*) ainsi que mon Nom (*GOMEZ Léo*) et un bouton pour télécharger mon CV que j'avais initialement appelé "*En savoir plus*" comme indiqué sur cette maquette mais que j'ai finalement renommé "*Mon CV*" pour une meilleure comprehension.
Le tout sur l'image d'un ordinateur avec une animation zoom.
Juste en dessous je voulais y mettre le liens vers mes réseaux sociaux *LinkedIn, Facebook et Instagram*.

Ensuite, j'ai voulu afficher mes compétences les plus importantes, en cercle autour d'une image représentant les compétences.

Juste en dessous je voulais faire apparaitre les grands projets que j'ai été amené a réaliser ainsi que les compétences dont j'ai eu besoin pour cela car c'est tout de même le but du Portfolio.
Pour chaque projet, je voulais créer une animation où, au passage de la souris, l'image d'arrière plan s'assombrit et un titre représentant le nom du projet ainsi que les grandes compétences nécéssaires à sa réalisation. Mais je voulais également faire apparaitre un petit paragraphe pour pitcher le contexte et le projet.
Ajouté a cela, j'ai décidé de rajouter des boutons a chaque projet qui renverraient ulterieurement vers une page sur laquelle nous aurions pu afficher le détails de chaque projet (Cette fonctionnalité ne fonctionne pas encore car je n'ai pas encore eu le temps de créer les différente pages, les boutons ne renvoient donc pour le moment à rien.).

En dessous de la section dédiée aux projets, j'ai voulu ajouter une nouvelle section de contact afin que toute personne intéréssée par mon profil puisse me contacter pour plusieurs eventuelles raisons.

Enfin pour faciliter la navigation de l'utilisateur je voulais rajouter un menu fixe en haut a droite de l'écran qui se déplacerait sur la page lorsque l'on descend sur cette dernière.
Toujours pour faciliter la navigation de l'utilisateur, j'ai pensé intéréssant de rajouter une petite flêche en bas de l'écran à droite qui lorsque l'on clique dessus nous renverrait vers le haut de la page.

Enfin afin que le site soit accessible sur tout appareil, j'ai décidé de le rendre responsive pour que tout le monde puisse y acceder de n'importe où par le biais de n'importe quel appareil.
